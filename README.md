# DO101-apps

Apps for the DO101 course.


## GITLAB Deploy Token

    Username: gitlab+deploy-token-164186
    Password: CcHjwJ5jKhwgYskKPzrq

## Neues Secret Erzeugen

    oc create secret generic gitlab-do101 --from-literal=username=gitlab+deploy-token-164186 --from-literal=password=CcHjwJ5jKhwgYskKPzrq --type=kubernetes.io/basic-auth

## Neues Secret durch Builder erzeugen lassen

    oc secrets link builder gitlab-do101

## Neue App erzeugen lassen - Branch: update-app 

    oc new-app --name version https://gitlab.com/mape/DO101.git#update-app --context-dir version --source-secret=gitlab-do101

## AUf ein bestimmtes Projekt wechseln

    oc project martin-petrak-bmlv-scale

## Wenn im Project die CPU auf über 20% geht dann weiteren Pod starten, bis max. 3 Pods

    oc autoscale dc/scale --max=3 --cpu-percent=20